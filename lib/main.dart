import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  void getData() async{
    final url = Uri.parse('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all');
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      print(response.body);
    }  else {
      print('error');
    }

  }

  @override
  Widget build(BuildContext context) {

    getData();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text('Flutter Rest API'),
        ),
      )
    );
  }
}

